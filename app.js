const express = require('express');
const app = express();
const path = require('path');
const morgan = require('morgan');

var bodyParser = require('body-parser');
var IBMIoTF = require('ibmiotf');

var router = require('./routes/index');

// Servidor express
const port = process.env.PORT || 8000;
app.listen(port, () => {
    console.log('Servidor online en el puerto', port);
})

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
var urlencoded = bodyParser.urlencoded({extended: false});

//Settings
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// HomePage
 app.use(router)


// ****************************************************************************
// Constructor
var appClientConfig = {
  org: '88zk0q',
  id: '88zk0q',
  "auth-key": 'a-88zk0q-hdni1u9nde',
  "auth-token": 'Drj(l&GJDlFnx+6@4G'
};
var appClient = new IBMIoTF.IotfApplication(appClientConfig);


//setting the log level to trace. By default its 'warn'
appClient.connect();
appClient.log.setLevel('trace');

appClient.on('connect', function(){
    console.log("connected");
    let types = [];

    router.post('/addDevice', (req, res, next) => {

        let body = req.body;
        let { typeDevice } = req.body;
        let { descType } = req.body;
        body.status = false;
        let type = body.typeDevice;
        let desc = body.descType;

        types.push({
            type,
            desc
        });

        //Register a new Device Type
         appClient.registerDeviceType(type,desc).then (function onSuccess (argument) {
            console.log("Success");
            console.log(argument);
        }, function onError (argument){
            console.log("Fail");
            console.log(argument);
        });
        console.log(body);
        console.log("Tipo: " + type);
        console.log("Descripción: " + desc);

        res.redirect('/');

    });

});

appClient.on('reconnect', function(){

	console.log("Reconnected!!!");
});

appClient.on('disconnect', function(){
  console.log('Disconnected from IoTF');
});

appClient.on('error', function (argument) {
	console.log(argument);
});

// Obtener todos los dispositivos

// appClient.
// getAllDevices(). then (function onSuccess (response) {
// 	console.log("Success");
// 	console.log("Number of devices : "+response.meta.total_rows);
// 	console.log(response);
// }, function onError (argument) {
//
// 	console.log("Fail");
// 	console.log(argument);
// });


// var type = "test";
// var desc = "prueba de registro";




//Register un tipo de dispositivo

// var type = "myDeviceType";
// var desc = "My Device Type"
// var metadata = {"customField1": "customValue3", "customField2": "customValue4"}
// var deviceInfo = {"serialNumber": "001", "manufacturer": "Blueberry", "model": "e2", "deviceClass": "A", "descriptiveLocation" : "Bangalore", "fwVersion" : "1.0.1", "hwVersion" : "12.01"}
//
// appClient.
// registerDeviceType(type,desc,deviceInfo,metadata).then (function onSuccess (argument) {
//         console.log("Success");
//         console.log(argument);
// }, function onError (argument) {
//
//         console.log("Fail");
//         console.log(argument);
// });


// Obtener detalles de la organización
// appClient.getOrganizationDetails().then (function onSuccess (response) {
//             console.log("Success");
//             console.log(response);
//     }, function onError (error) {
//
//             console.log("Fail");
//             console.log(error);
//     });

// Obtener todos los tipos de dispositivos
// appClient.getAllDeviceTypes().then (function (response) {
//     console.log("Success");
//     console.log(response);
// }, function onError (error) {
//
//     console.log("Fail");
//     console.log(error);
// });



//List all devices of Device Type 'xxxx'
// appClient.
// listAllDevicesOfType('gateway').then (function onSuccess (argument) {
// 	console.log("Success");
// 	console.log(argument.results);
// }, function onError (argument) {
//
// 	console.log("Fail");
// 	console.log(argument);
// });


// Agregar un dispositivo
//
// var type = "temperaturaturita";
// var deviceId = "tempAmbiebre";
// var authToken = "Kineticsdev2018*";
//
// appClient.
// registerDevice(type, deviceId, authToken).then (function onSuccess (dispositivo) {
//     console.log("Success");
//     console.log(dispositivo);
// }, function onError (error){
//     console.log("Fail");
//     console.log(error.data);
// });


//Register un tipo de dispositivo
//
// var type = "temperatura";
// var description = "tempreatura del aire";
//
// appClient.
// registerDeviceType(type,description).then (function onSuccess (res) {
// 	console.log("Success");
// 	console.log(res);
//     let typeid = res.id;
// }, function onError (res) {
//
// 	console.log("Fail");
// 	console.log(res);
// });
